from django.db import models

# Create your models here.
GENDER_CHOICES = (('m', 'male'), ('f', 'female'), ('o', 'other'))


class Teams(models.Model):
    name = models.CharField(max_length=25)
    password = models.CharField(max_length=64)
    billing_hour = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Employee(models.Model):
    name = models.CharField(max_length=25)
    mid_name = models.CharField(max_length=25)
    surname = models.CharField(max_length=25)
    dob = models.DateField()
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    address = models.CharField(max_length=50)
    phone = models.CharField(max_length=15)
    email = models.EmailField()
    work_start_time = models.TimeField()
    work_end_time = models.TimeField()
    job_position = models.CharField(max_length=25)
    team = models.ForeignKey(Teams, on_delete=models.CASCADE, blank=True, null=True)
    billing_hour = models.IntegerField()
    profile_pic = models.ImageField(upload_to='profile_pic', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
