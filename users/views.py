from rest_framework import viewsets

from users.models import Teams, Employee
from users.serializers import TeamsSerializer


# Create your views here.
class TeamsViewSet(viewsets.ModelViewSet):
    queryset = Teams.objects.all()
    serializer_class = TeamsSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = TeamsSerializer
